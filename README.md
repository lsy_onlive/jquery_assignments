# jquery_assignments

#### 介绍
jquery前端设计课程作业仓库

#### 关于作业仓库的使用说明
##### 个人作业仓库建立
+ 在[https://gitee.com/login](码云)注册一个账号并登陆;
+ 打开教师的作业仓库[https://gitee.com/jgy-gzsy_jacklisp/jquery_assignments](javascript_assignments),点击右上角
  的fork到自己的分支,此后作业可以在自己的分支底下修改; 
+ 安装git,并将git的安装目录下的bin目录添加到环境变量;
+ 确保在vscode的终端能够调用git;
+ 在vscode安装GitLens插件;
##### 个人作业提交流程
+ 将自己的分支克隆到本地
```
git clone https://gitee.com/xxxxxx/jquery_assignments.git
```
+ 在本地完成修改并调试通过
+ 使用如下命令提交到本地仓库并推送到远程个人仓库
```
git add .
git commit -m "message"
git push
```
+ 在浏览器页面登陆[https://gitee.com/login](码云)
+ 创建一个Pull Request申请提交到教师仓库
+ 等候教师的批改,教师批改完成后会对pr加评语、关闭pr、完成作业记录,如评语中显示作业未通过，则继续完成作业再次提交
+ 注：为了保证教师作业仓库不被破坏，教师不会对作业仓库进行合并
+ 注: 教师作业仓库会不定期更新，使用如下命令将作业仓库更新到自己的分支，必要情况下需要进行冲突合并
```
git pull https://gitee.com/jgy-gzsy_jacklisp/jquery_assignments.git [你的分支名：一般为master]
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 关于签到
+ 系部启用了微信签到系统，签到地址[wx.gxstu.cn](wx.gxstu.cn)
+ 该系统签到结果直接上传到系部，届时系部领导、辅导员、班主任均可以动态查看
+ 学生签到需要使用微信扫描教师在投影或屏幕广播上展示的动态二维码进行签到。第一次签到需要输入学号和身份证绑定

#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
